"use strict";

jQuery(function ($) {
  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false
  });
  $('.k-qa-card__header').on('click', function (e) {
    e.preventDefault();
    $(this).parent().toggleClass('k-qa-card_active');
    $(this).next().slideToggle('fast');
  });
  $('.open-modal').on('click', function (e) {
    e.preventDefault();
    $('.k-modal').toggle();
  });
  $('.k-modal__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'k-modal__centered') {
      $('.k-modal').hide();
    }
  });
  $('.k-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.k-modal').hide();
  });
  $.bvi({
    bvi_target: '.bvi-open',
    // Класс ссылки включения плагина
    bvi_theme: 'white',
    // Цвет сайта
    bvi_font: 'arial',
    // Шрифт
    bvi_font_size: 16,
    // Размер шрифта
    bvi_letter_spacing: 'normal',
    // Межбуквенный интервал
    bvi_line_height: 'normal',
    // Междустрочный интервал
    bvi_images: true,
    // Изображения
    bvi_reload: false,
    // Перезагрузка страницы при выключении плагина
    bvi_fixed: false,
    // Фиксирование панели для слабовидящих вверху страницы
    bvi_tts: true,
    // Синтез речи
    bvi_flash_iframe: true,
    // Встроенные элементы (видео, карты и тд.)
    bvi_hide: false // Скрывает панель для слабовидящих и показывает иконку панели.

  });
  new Swiper('.k-doctors__cards', {
    navigation: {
      nextEl: '.k-doctors .swiper-button-next',
      prevEl: '.k-doctors .swiper-button-prev'
    },
    loop: true,
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      1200: {
        slidesPerView: 2
      }
    }
  });
  new Swiper('.k-items__cards', {
    navigation: {
      nextEl: '.k-items .swiper-button-next',
      prevEl: '.k-items .swiper-button-prev'
    },
    loop: true,
    spaceBetween: 30,
    slidesPerView: 1
  });
  new Swiper('.k-reviews__cards', {
    navigation: {
      nextEl: '.k-reviews .swiper-button-next',
      prevEl: '.k-reviews .swiper-button-prev'
    },
    loop: true,
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      1200: {
        slidesPerView: 2
      }
    }
  });
  var bar = new ProgressBar.Circle(progress, {
    strokeWidth: 1,
    easing: 'easeInOut',
    duration: 500,
    color: '#0E72BA',
    trailColor: '#EBF0F3',
    trailWidth: 1,
    svgStyle: null,
    step: function step(state, bar) {
      $('.a-home-features-progress__title').html(Math.round(bar.value() * 100) + ' %');
    }
  });
  bar.animate(0.25);
  var galleryThumbs = new Swiper('.gallery-thumbs', {
    spaceBetween: 30,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    pagination: {
      el: '.k-steps .swiper-pagination',
      type: 'fraction'
    },
    allowTouchMove: false
  });
  var galleryTop = new Swiper('.gallery-top', {
    effect: 'fade',
    spaceBetween: 30,
    thumbs: {
      swiper: galleryThumbs
    },
    navigation: {
      nextEl: '.k-steps .swiper-button-next',
      prevEl: '.k-steps .swiper-button-prev'
    }
  });
  galleryTop.on('slideChange', function () {
    if (this.realIndex === 0) {
      $('.k-steps__number').each(function (i) {
        $(this).removeClass('k-steps__number_active');
      });
      $('.k-steps__number_1').addClass('k-steps__number_active');
      bar.animate(0.25);
    } else if (this.realIndex === 1) {
      $('.k-steps__number').each(function (i) {
        $(this).removeClass('k-steps__number_active');
      });
      $('.k-steps__number_1').addClass('k-steps__number_active');
      $('.k-steps__number_2').addClass('k-steps__number_active');
      bar.animate(0.5);
    } else if (this.realIndex === 2) {
      $('.k-steps__number').each(function (i) {
        $(this).removeClass('k-steps__number_active');
      });
      $('.k-steps__number_1').addClass('k-steps__number_active');
      $('.k-steps__number_2').addClass('k-steps__number_active');
      $('.k-steps__number_3').addClass('k-steps__number_active');
      bar.animate(0.75);
    } else if (this.realIndex === 3) {
      $('.k-steps__number').each(function (i) {
        $(this).removeClass('k-steps__number_active');
      });
      $('.k-steps__number_1').addClass('k-steps__number_active');
      $('.k-steps__number_2').addClass('k-steps__number_active');
      $('.k-steps__number_3').addClass('k-steps__number_active');
      $('.k-steps__number_4').addClass('k-steps__number_active');
      bar.animate(1);
    }
  });
  var scene = document.getElementById('scene');
  var parallaxInstance = new Parallax(scene);
});
//# sourceMappingURL=main.js.map